# Магазин кошачего корма "Кэт энерджи"

*Проект импортирован с [GitHub](https://github.com/dpsnow/574945-cat-energy).*

Проект выполнен в рамках интенсива «[Профессиональный HTML и CSS, уровень 2»](https://htmlacademy.ru/intensive/htmlcss) (c 9 апреля по 16 мая 2018) от [HTML Academy](https://htmlacademy.ru).

Сверстаны 3 страницы: [Главная](https://deep_snow.gitlab.io/cat-energy/index.html), [Каталог продукции](https://deep_snow.gitlab.io/cat-energy/catalog.html) и [Подбор программы](https://deep_snow.gitlab.io/cat-energy/form.html).

- Фиксированный адаптивный сайт (мобильная, планшетная и десктопная версии).
- Стратегия Mobile First.
- Сетка на flexbox.
- Наименование стилей по БЭМ.
- Препроцессор SASS.
- Адаптивные изображения, ретинизация.
- Сборка на Gulp.
- Знакомство с JS (раскрывающееся меню, попытка реализовать слайдер (до/после)).
